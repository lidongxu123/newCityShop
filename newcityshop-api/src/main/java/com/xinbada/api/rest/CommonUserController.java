package com.xinbada.api.rest;

import co.xinbada.enums.RedisKeys;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("用户")
public class CommonUserController {

//    @Autowired
//    private AppSmsService appSmsService;

    @ApiOperation(value = "发送手机号注册验证码")
    @GetMapping("sendRegisterSmsCode")
    public void sendRegisterSmsCode(
            @ApiParam("手机号") @RequestParam(required = true) String phone){
        String redisKey = RedisKeys.getLoginSmsCode(phone);
        String redisKey1Min = RedisKeys.getLoginSmsCode1Min(phone);
        //appSmsService.checkSendSmsCode(phone, redisKey1Min);


    }


}
