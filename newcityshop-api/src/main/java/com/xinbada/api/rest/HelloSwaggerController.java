package com.xinbada.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "测试swagger")
@RequestMapping("/hello")
public class HelloSwaggerController {

    @GetMapping
    @ApiOperation("api接口测试")
    public String restSwagger(){
        return "hahhaa";
    }

}
