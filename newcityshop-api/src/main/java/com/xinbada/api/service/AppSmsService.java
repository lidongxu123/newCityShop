/*
package com.xinbada.modules.service;

import co.xinbada.enums.RedisKeys;
import co.xinbada.modules.sms.service.TencentSmsRecordService;
import co.xinbada.tools.domain.TencentSmsRecord;
import HttpKit;
import TencentSmsKit;
import TencentAccount;
import TencentSmsRequest;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Service
public class AppSmsService {

    private static TencentAccount tencentAccount = new TencentAccount();

    private static final String SmsRegisterTemplateId = "568950";
    private static final String SmsResetPasswordTemplateId = "568955";
    private static final String SmsLoginTemplateId = "568943";
    private static final String SmsBindPhoneTemplateId = "585982";

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    //private TencentSmsRecordService tencentSmsRecordService;

    public ResponseEntity checkSendSmsCode(String phone, String redisKey1Min) {
        // 检查是否有参数手机号
        if (StringUtils.isBlank(phone)) {
            return new ResponseEntity("手机号码不能为空", HttpStatus.NOT_FOUND);
        }

        // 手机号是否有效
        phone = phone.trim();
        boolean isPhone = Pattern.compile("1\\d{10}").matcher(phone).matches();
        if (!isPhone) {
            return new ResponseEntity("电话格式不合法",HttpStatus.NOT_ACCEPTABLE);

        }

        // 检查是否可以重发 （判断同一手机号， 1分钟内不可重复发短信）
//		String registCodeResendKey = RedisKeys.getRegistSmsCode1Min(phone);
        String registCode = stringRedisTemplate.opsForValue().get(redisKey1Min);
        if (StringUtils.isNotBlank(registCode)) {
            return new ResponseEntity("1分钟之内不允许重发",HttpStatus.NOT_ACCEPTABLE);
        }

        // 检查是否可以重发（判断最近一个小时的发送总数， 根据同手机号判断）
        Date date = DateUtils.addMinutes(new Date(), -60);
        long count = countByPhoneAfterTime(phone, date);
        if (count >= 10) {
            return new ResponseEntity("操作频繁，稍后重试!",HttpStatus.NOT_ACCEPTABLE);
        }

        // 检查是否可以重发（判断同一IP一天内的发送数）
        Date yesterday = DateUtils.addDays(new Date(), -1);
        String ip = HttpKit.getClientIp();
        long dayCount = countByIpAfterTime(ip, yesterday);
        if (dayCount >= 20) {
            return new ResponseEntity("操作频繁，稍后重试!",HttpStatus.NOT_ACCEPTABLE);
        }
        return null; // check pass
    }

    public int countByPhoneAfterTime(String phone, Date time) {
        return 0;
    }

    public int countByIpAfterTime(String ip, Date time) {
        return 0;
    }

    */
/**
     * 生成验证码
     *
     * @param redisKey
     * @return
     *//*

    public String genCode(String redisKey) {
        String code = stringRedisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isBlank(code)) {
            code = RandomStringUtils.random(6, false, true);
        }
        return code;
    }

    */
/**
     * 发送登录验证码
     *
     * @param phone
     * @param code
     *//*

    public void sendLoginSmsCode(String phone, String code) {
        TencentSmsRequest smsRequest = TencentSmsRequest.getDefault();
        smsRequest.setParams(new String[]{code});
        smsRequest.setPhone(phone);
        smsRequest.setPurpose("login");
        smsRequest.setTemplateId(SmsLoginTemplateId);
        TencentSmsRecord record = TencentSmsKit.sendSms(tencentAccount, smsRequest);
        tencentSmsRecordService.save(record);
        if ("Ok".equals(record.getResponseCode())) {
            String redisKey = RedisKeys.getLoginSmsCode(phone);
            String redisKey1Min = RedisKeys.getLoginSmsCode1Min(phone);
            stringRedisTemplate.opsForValue().set(redisKey, code, 10L, TimeUnit.MINUTES);
            stringRedisTemplate.opsForValue().set(redisKey1Min, code, 1, TimeUnit.MINUTES);
        }
    }

    */
/**
     * 发送注册短信验证
     *
     * @param phone
     * @param code
     *//*

    public void sendRegistSmsCode(String phone, String code) {
        TencentSmsRequest smsRequest = TencentSmsRequest.getDefault();
        smsRequest.setParams(new String[]{code});
        smsRequest.setPhone(phone);
        smsRequest.setPurpose("register");
        smsRequest.setTemplateId(SmsRegisterTemplateId);
        TencentSmsRecord record = TencentSmsKit.sendSms(tencentAccount, smsRequest);
        tencentSmsRecordService.save(record);
        if ("Ok".equals(record.getResponseCode())) {
            String redisKey = RedisKeys.getRegistSmsCode(phone);
            String redisKey1Min = RedisKeys.getRegistSmsCode1Min(phone);
            stringRedisTemplate.opsForValue().set(redisKey, code, 10L, TimeUnit.MINUTES);
            stringRedisTemplate.opsForValue().set(redisKey1Min, code, 1, TimeUnit.MINUTES);
        }
    }

    */
/**
     * 发送重置短信验证码
     *
     * @param phone
     * @param code
     *//*

    public void sendResetPasswordSmsCode(String phone, String code) {
        TencentSmsRequest smsRequest = TencentSmsRequest.getDefault();
        smsRequest.setParams(new String[]{code});
        smsRequest.setPhone(phone);
        smsRequest.setPurpose("resetPassword");
        smsRequest.setTemplateId(SmsResetPasswordTemplateId);
        TencentSmsRecord record = TencentSmsKit.sendSms(tencentAccount, smsRequest);
        tencentSmsRecordService.save(record);
        if ("Ok".equals(record.getResponseCode())) {
            String redisKey = RedisKeys.getResetPasswordSmsCode(phone);
            String redisKey1Min = RedisKeys.getResetPasswordSmsCode1Min(phone);
            stringRedisTemplate.opsForValue().set(redisKey, code, 10L, TimeUnit.MINUTES);
            stringRedisTemplate.opsForValue().set(redisKey1Min, code, 1, TimeUnit.MINUTES);
        }
    }

    */
/**
     * 发送绑定手机短信验证码
     * @param phone
     * @param code
     *//*

    public void sendBindPhoneSmsCode(String phone, String code) {
        TencentSmsRequest smsRequest = TencentSmsRequest.getDefault();
        smsRequest.setParams(new String[]{code});
        smsRequest.setPhone(phone);
        smsRequest.setPurpose("bindPhone");
        smsRequest.setTemplateId(SmsBindPhoneTemplateId);
        TencentSmsRecord record = TencentSmsKit.sendSms(tencentAccount, smsRequest);
        tencentSmsRecordService.save(record);
        if("Ok".equals(record.getResponseCode())) {
            String redisKey = RedisKeys.getBindPhoneSmsCode(phone);
            String redisKey1Min = RedisKeys.getBindPhoneSmsCode1Min(phone);
            stringRedisTemplate.opsForValue().set(redisKey, code, 10L, TimeUnit.MINUTES);
            stringRedisTemplate.opsForValue().set(redisKey1Min, code, 1, TimeUnit.MINUTES);
        }
    }





}
*/
