package co.xinbada.enums;

public class RedisKeys {

	private static final String SmsCode = "smsCode:";

	public static String getLoginSmsCode(String phone) {
		return SmsCode+"login:"+phone;
	}
	public static String getLoginSmsCode1Min(String phone) {
		return SmsCode+"login.1min:"+phone;
	}

	public static String getRegistSmsCode(String phone) {
		return SmsCode+"regist:"+phone;
	}
	public static String getRegistSmsCode1Min(String phone) {
		return SmsCode+"regist.1min:"+phone;
	}


	public static String getResetPasswordSmsCode(String phone) {
		return SmsCode+"resetPassword:"+phone;
	}
	public static String getResetPasswordSmsCode1Min(String phone) {
		return SmsCode+"resetPassword.1min:"+phone;
	}


	public static String getBindPhoneSmsCode(String phone) {
		return SmsCode+"bindPhone:"+phone;
	}
	public static String getBindPhoneSmsCode1Min(String phone) {
		return SmsCode+"bindPhone.1min:"+phone;
	}

}
