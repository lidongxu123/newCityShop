/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.xinbada.co

 */
package co.xinbada.api.service;

import co.xinbada.api.service.dto.YxArticleDto;
import co.xinbada.api.service.dto.YxArticleQueryCriteria;
import co.xinbada.common.service.BaseService;
import co.xinbada.api.domain.YxArticle;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
* @author hupeng
* @date 2020-05-12
*/
public interface YxArticleService  extends BaseService<YxArticle>{

/**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(YxArticleQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<YxArticleDto>
    */
    List<YxArticle> queryAll(YxArticleQueryCriteria criteria);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<YxArticleDto> all, HttpServletResponse response) throws IOException;

    void uploadNews(YxArticleDto yxArticleDTO) throws WxErrorException;
}
