/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.xinbada.co

 */
package co.xinbada.api.service.mapper;

import co.xinbada.common.mapper.CoreMapper;
import co.xinbada.api.domain.YxWechatMenu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* @author hupeng
* @date 2020-05-12
*/
@Repository
@Mapper
public interface WechatMenuMapper extends CoreMapper<YxWechatMenu> {

}
