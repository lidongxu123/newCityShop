/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.xinbada.co

 */
package co.xinbada.modules.activity.service.dto;

import co.xinbada.annotation.Query;
import lombok.Data;

/**
* @author hupeng
* @date 2020-05-13
*/
@Data
public class YxUserExtractQueryCriteria{


    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String realName;
}
