/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.xinbada.co

 */
package co.xinbada.modules.activity.service.mapper;

import co.xinbada.common.mapper.CoreMapper;
import co.xinbada.modules.activity.domain.YxStoreBargain;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* @author hupeng
* @date 2020-05-13
*/
@Repository
@Mapper
public interface YxStoreBargainMapper extends CoreMapper<YxStoreBargain> {

}
