package co.xinbada.modules.kit;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Objects;

public class HttpKit {

	public static HttpServletRequest request() {
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if(servletRequestAttributes != null) {
			return servletRequestAttributes.getRequest();
		} else {
			return null;
		}
	}

	public static HttpServletResponse response() {
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if(servletRequestAttributes != null) {
			return servletRequestAttributes.getResponse();
		} else {
			return null;
		}
	}

	public static HttpSession session() {
		HttpServletRequest httpServletRequest = request();
		if(httpServletRequest != null) {
			return httpServletRequest.getSession();
		} else {
			return null;
		}
	}

	public static String getIp() {
		HttpServletRequest request = request();
		if(request == null) {
			return null;
		} else {
			return request.getRemoteAddr();
		}
	}

	public static Cookie getCookie(String name) {
		if(StringUtils.isBlank(name)) {
			return null;
		}
		HttpServletRequest request = request();
		if(request == null) {
			return null;
		}

		Cookie[] cookieArray = request.getCookies();
		if(cookieArray != null) {
			for(Cookie cookie : cookieArray) {
				if(Objects.equals(name, cookie.getName())) {
					return cookie;
				}
			}
		}
		return null;
	}

	public static String getCookieValue(String cookieName) {
		Cookie cookie = getCookie(cookieName);
		if(cookie != null) {
			return cookie.getValue();
		}
		return null;
	}

	public static String getToken() {
		HttpServletRequest request = request();
		if(request == null) {
			return null;
		}

		String cookieToken = getCookieValue("token");
		String headerToken = request.getHeader("token");

		return StringUtils.isNotBlank(headerToken) ?  headerToken : cookieToken;
	}

	public static String getClientIp(HttpServletRequest httpServletRequest) {
		String remoteAddr = httpServletRequest.getRemoteAddr();
		String xForwardedFor = httpServletRequest.getHeader("X-Forwarded-For");
		if(StringUtils.isNotBlank(xForwardedFor) && !"unknown".equalsIgnoreCase(xForwardedFor)) {
			String[] ips = xForwardedFor.split(",");
			if(ips.length>0) {
				return ips[0];
			}
		}

		String xRealIp = httpServletRequest.getHeader("X-Real-IP");
		if (StringUtils.isNotBlank(xRealIp) && !"unKnown".equalsIgnoreCase(xRealIp)) {
			return xRealIp;
		}

		return httpServletRequest.getRemoteAddr();
	}

	public static String getClientIp() {
		HttpServletRequest httpServletRequest = request();
		String ip = getClientIp(httpServletRequest);
		return ip;
	}

	public static void responseJson(HttpServletResponse response, String jsonString) {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.append(jsonString);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(out != null) {
				out.flush();
			}
		}
	}

	public static String header(String key) {
		return request().getHeader(key);
	}

	public static String getRequestInputStreamAsString() {
		try {
			HttpServletRequest request = request();
			request.setCharacterEncoding("UTF-8");
			BufferedReader br=new BufferedReader(new InputStreamReader((ServletInputStream)request.getInputStream(),"utf-8"));
			StringBuffer sb=new StringBuffer("");
			String temp;
			while((temp=br.readLine())!=null){
				sb.append(temp);//把接收的参数赋给sb字符串
			}
			br.close();

			String string = sb.toString();
			return string;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}