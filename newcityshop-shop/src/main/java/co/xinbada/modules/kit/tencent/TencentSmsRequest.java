package co.xinbada.modules.kit.tencent;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TencentSmsRequest {

	private String purpose;
	private String sign;
	private String templateId;

	private String smsSdkAppId;

	private String phone;
	private String[] params;

	public static TencentSmsRequest getDefault() {
		TencentSmsRequest request = new TencentSmsRequest();
		request.setSmsSdkAppId("1400345187");
		request.setSign("小乐汽配");
		return request;
	}

}
