/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.xinbada.co

 */
package co.xinbada.modules.shop.service;

import co.xinbada.common.service.BaseService;
import co.xinbada.modules.shop.domain.YxStoreOrderStatus;
import co.xinbada.modules.shop.service.dto.YxStoreOrderStatusDto;
import co.xinbada.modules.shop.service.dto.YxStoreOrderStatusQueryCriteria;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
* @author hupeng
* @date 2020-05-12
*/
public interface YxStoreOrderStatusService  extends BaseService<YxStoreOrderStatus>{

/**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(YxStoreOrderStatusQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<YxStoreOrderStatusDto>
    */
    List<YxStoreOrderStatus> queryAll(YxStoreOrderStatusQueryCriteria criteria);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<YxStoreOrderStatusDto> all, HttpServletResponse response) throws IOException;
}
