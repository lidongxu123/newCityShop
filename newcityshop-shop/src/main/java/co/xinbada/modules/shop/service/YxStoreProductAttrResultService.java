/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.xinbada.co

 */
package co.xinbada.modules.shop.service;

import co.xinbada.common.service.BaseService;
import co.xinbada.modules.shop.domain.YxStoreProductAttrResult;

/**
* @author hupeng
* @date 2020-05-12
*/
public interface YxStoreProductAttrResultService  extends BaseService<YxStoreProductAttrResult>{

}
