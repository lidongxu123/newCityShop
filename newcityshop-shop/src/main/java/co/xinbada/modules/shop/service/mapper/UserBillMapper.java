/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.xinbada.co

 */
package co.xinbada.modules.shop.service.mapper;

import co.xinbada.common.mapper.CoreMapper;
import co.xinbada.modules.shop.domain.YxUserBill;
import co.xinbada.modules.shop.service.dto.YxUserBillDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author hupeng
* @date 2020-05-12
*/
@Repository
@Mapper
public interface UserBillMapper extends CoreMapper<YxUserBill> {

    @Select("<script> select b.title,b.pm,b.category,b.type,b.number,b.add_time ,u.nickname " +
            "from yx_user_bill b left join yx_user u on u.uid=b.uid  where 1=1  "  +
            "<if test =\"category !=''\">and b.category=#{category}</if> " +
            "<if test =\"type !=''\">and b.type=#{type}</if> " +
            "<if test =\"nickname !=''\">and u.nickname= LIKE CONCAT('%',#{nickname},'%')</if> </script> ")
    List<YxUserBillDto> findAllByQueryCriteria(@Param("category") String category, @Param("type") String type, @Param("nickname") String nickname);
}
