/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.sms.domain;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;
import java.util.Date;

/**
* @author 李东旭
* @date 2020-06-27
*/
@Data
@TableName("tencent_sms_record")
@Accessors(chain = true)
public class TencentSmsRecord implements Serializable {

    /** id */
    @TableId
    private Long id;


    /** 用途 */
    private String purpose;


    /** 记录 */
    private String sign;


    /** 请求id */
    private String requestId;


    /** 电话 */
    private String phone;


    /** 参数 */
    private String params;


    /** 响应费用 */
    private Integer responseFee;


    /** 响应码 */
    private String responseCode;


    /** 响应消息 */
    private String responseMessage;


    /** 创建时间 */
    @TableField(fill= FieldFill.INSERT)
    private Date createTime;


    public void copy(TencentSmsRecord source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
