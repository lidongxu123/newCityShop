/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.sms.rest;
import java.util.Arrays;
import co.xinbada.dozer.service.IGenerator;
import co.xinbada.modules.sms.domain.TencentSmsRecord;
import lombok.AllArgsConstructor;
import co.xinbada.logging.aop.log.Log;
import co.xinbada.modules.sms.service.TencentSmsRecordService;
import co.xinbada.modules.sms.service.dto.TencentSmsRecordQueryCriteria;
import co.xinbada.modules.sms.service.dto.TencentSmsRecordDto;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @author 李东旭
* @date 2020-06-27
*/
@AllArgsConstructor
@Api(tags = "modules/sms管理")
@RestController
@RequestMapping("/api/tencentSmsRecord")
public class TencentSmsRecordController {

    private final TencentSmsRecordService tencentSmsRecordService;
    private final IGenerator generator;


    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('admin','tencentSmsRecord:list')")
    public void download(HttpServletResponse response, TencentSmsRecordQueryCriteria criteria) throws IOException {
        tencentSmsRecordService.download(generator.convert(tencentSmsRecordService.queryAll(criteria), TencentSmsRecordDto.class), response);
    }

    @GetMapping
    @Log("查询api/sms")
    @ApiOperation("查询api/sms")
    @PreAuthorize("@el.check('admin','tencentSmsRecord:list')")
    public ResponseEntity<Object> getTencentSmsRecords(TencentSmsRecordQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(tencentSmsRecordService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增api/sms")
    @ApiOperation("新增api/sms")
    @PreAuthorize("@el.check('admin','tencentSmsRecord:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody TencentSmsRecord resources){
        return new ResponseEntity<>(tencentSmsRecordService.save(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改api/sms")
    @ApiOperation("修改api/sms")
    @PreAuthorize("@el.check('admin','tencentSmsRecord:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody TencentSmsRecord resources){
        tencentSmsRecordService.updateById(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除api/sms")
    @ApiOperation("删除api/sms")
    @PreAuthorize("@el.check('admin','tencentSmsRecord:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Long[] ids) {
        Arrays.asList(ids).forEach(id->{
            tencentSmsRecordService.removeById(id);
        });
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
