/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.sms.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author 李东旭
* @date 2020-06-27
*/
@Data
public class TencentSmsRecordDto implements Serializable {

    /** id */
    private Long id;

    /** 用途 */
    private String purpose;

    /** 记录 */
    private String sign;

    /** 请求id */
    private String requestId;

    /** 电话 */
    private String phone;

    /** 参数 */
    private String params;

    /** 响应费用 */
    private Integer responseFee;

    /** 响应码 */
    private String responseCode;

    /** 响应消息 */
    private String responseMessage;

    /** 创建时间 */
    private Timestamp createTime;
}
