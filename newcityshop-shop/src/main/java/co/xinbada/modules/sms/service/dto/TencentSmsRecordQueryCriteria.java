/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.sms.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.util.List;
import co.xinbada.annotation.Query;

/**
* @author 李东旭
* @date 2020-06-27
*/
@Data
public class TencentSmsRecordQueryCriteria{

    /** 精确 */
    @Query
    private String purpose;

    /** 模糊 */
    @Query(type = Query.Type.INNER_LIKE)
    private String sign;
    /** BETWEEN */
    @Query(type = Query.Type.BETWEEN)
    private List<Timestamp> createTime;
}
