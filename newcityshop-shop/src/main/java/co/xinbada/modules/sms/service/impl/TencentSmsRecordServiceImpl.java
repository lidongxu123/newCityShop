/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.sms.service.impl;

import co.xinbada.modules.sms.domain.TencentSmsRecord;
import co.xinbada.common.service.impl.BaseServiceImpl;
import lombok.AllArgsConstructor;
import co.xinbada.dozer.service.IGenerator;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import co.xinbada.common.utils.QueryHelpPlus;
import co.xinbada.utils.ValidationUtil;
import co.xinbada.utils.FileUtil;
import co.xinbada.modules.sms.service.TencentSmsRecordService;
import co.xinbada.modules.sms.service.dto.TencentSmsRecordDto;
import co.xinbada.modules.sms.service.dto.TencentSmsRecordQueryCriteria;
import co.xinbada.modules.sms.service.mapper.TencentSmsRecordMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @author 李东旭
* @date 2020-06-27
*/
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "tencentSmsRecord")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TencentSmsRecordServiceImpl extends BaseServiceImpl<TencentSmsRecordMapper, TencentSmsRecord> implements TencentSmsRecordService {

    private final IGenerator generator;

    @Override
    //@Cacheable
    public Map<String, Object> queryAll(TencentSmsRecordQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<TencentSmsRecord> page = new PageInfo<>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), TencentSmsRecordDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }


    @Override
    //@Cacheable
    public List<TencentSmsRecord> queryAll(TencentSmsRecordQueryCriteria criteria){
        return baseMapper.selectList(QueryHelpPlus.getPredicate(TencentSmsRecord.class, criteria));
    }


    @Override
    public void download(List<TencentSmsRecordDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (TencentSmsRecordDto tencentSmsRecord : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("用途", tencentSmsRecord.getPurpose());
            map.put("记录", tencentSmsRecord.getSign());
            map.put("请求id", tencentSmsRecord.getRequestId());
            map.put("电话", tencentSmsRecord.getPhone());
            map.put("参数", tencentSmsRecord.getParams());
            map.put("响应费用", tencentSmsRecord.getResponseFee());
            map.put("响应码", tencentSmsRecord.getResponseCode());
            map.put("响应消息", tencentSmsRecord.getResponseMessage());
            map.put("创建时间", tencentSmsRecord.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}
