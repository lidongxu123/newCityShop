/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.sms.service.mapper;

import co.xinbada.common.mapper.CoreMapper;
import co.xinbada.modules.sms.domain.TencentSmsRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* @author 李东旭
* @date 2020-06-27
*/
@Repository
@Mapper
public interface TencentSmsRecordMapper extends CoreMapper<TencentSmsRecord> {

}
