/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.system.rest;
import java.util.Arrays;
import co.xinbada.dozer.service.IGenerator;
import lombok.AllArgsConstructor;
import co.xinbada.logging.aop.log.Log;
import co.xinbada.modules.system.domain.Comment;
import co.xinbada.modules.system.service.CommentService;
import co.xinbada.modules.system.service.dto.CommentQueryCriteria;
import co.xinbada.modules.system.service.dto.CommentDto;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @author lidongxu
* @date 2020-06-23
*/
@AllArgsConstructor
@Api(tags = "modules/comment管理")
@RestController
@RequestMapping("/api/comment")
public class CommentController {

    private final CommentService commentService;
    private final IGenerator generator;


    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('admin','comment:list')")
    public void download(HttpServletResponse response, CommentQueryCriteria criteria) throws IOException {
        commentService.download(generator.convert(commentService.queryAll(criteria), CommentDto.class), response);
    }

    @GetMapping
    @Log("查询api/comment")
    @ApiOperation("查询api/comment")
    @PreAuthorize("@el.check('admin','comment:list')")
    public ResponseEntity<Object> getComments(CommentQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(commentService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增api/comment")
    @ApiOperation("新增api/comment")
    @PreAuthorize("@el.check('admin','comment:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Comment resources){
        return new ResponseEntity<>(commentService.save(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改api/comment")
    @ApiOperation("修改api/comment")
    @PreAuthorize("@el.check('admin','comment:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Comment resources){
        commentService.updateById(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除api/comment")
    @ApiOperation("删除api/comment")
    @PreAuthorize("@el.check('admin','comment:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Long[] ids) {
        Arrays.asList(ids).forEach(id->{
            commentService.removeById(id);
        });
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
