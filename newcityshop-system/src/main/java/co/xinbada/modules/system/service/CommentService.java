/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.system.service;
import co.xinbada.common.service.BaseService;
import co.xinbada.modules.system.domain.Comment;
import co.xinbada.modules.system.service.dto.CommentDto;
import co.xinbada.modules.system.service.dto.CommentQueryCriteria;
import org.springframework.data.domain.Pageable;
import java.util.Map;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @author lidongxu
* @date 2020-06-23
*/
public interface CommentService  extends BaseService<Comment>{

/**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(CommentQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<CommentDto>
    */
    List<Comment> queryAll(CommentQueryCriteria criteria);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<CommentDto> all, HttpServletResponse response) throws IOException;
}
