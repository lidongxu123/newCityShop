/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.system.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author lidongxu
* @date 2020-06-23
*/
@Data
public class CommentDto implements Serializable {

    /** 意见ID */
    private Long id;

    /** 用户反馈内容 */
    private String comment;

    /** 创建时间 */
    private Timestamp createTime;

    /** 创建人 */
    private String createUser;

    /** 修改人 */
    private String updateUser;

    /** 修改时间 */
    private Timestamp updateTime;

    /** 删除：0->默认；1->删除 */
    private Integer isDelete;

    /** 状态：0->默认；1->禁用 */
    private Integer status;
}
