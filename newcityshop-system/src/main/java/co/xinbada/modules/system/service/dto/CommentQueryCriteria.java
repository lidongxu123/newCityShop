/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.system.service.dto;

import lombok.Data;
import java.util.List;
import co.xinbada.annotation.Query;

/**
* @author lidongxu
* @date 2020-06-23
*/
@Data
public class CommentQueryCriteria{
}
