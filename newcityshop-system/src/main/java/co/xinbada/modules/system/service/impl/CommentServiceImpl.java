/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.system.service.impl;

import co.xinbada.modules.system.domain.Comment;
import co.xinbada.common.service.impl.BaseServiceImpl;
import lombok.AllArgsConstructor;
import co.xinbada.dozer.service.IGenerator;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import co.xinbada.common.utils.QueryHelpPlus;
import co.xinbada.utils.ValidationUtil;
import co.xinbada.utils.FileUtil;
import co.xinbada.modules.system.service.CommentService;
import co.xinbada.modules.system.service.dto.CommentDto;
import co.xinbada.modules.system.service.dto.CommentQueryCriteria;
import co.xinbada.modules.system.service.mapper.CommentMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @author lidongxu
* @date 2020-06-23
*/
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "comment")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class CommentServiceImpl extends BaseServiceImpl<CommentMapper, Comment> implements CommentService {

    private final IGenerator generator;

    @Override
    //@Cacheable
    public Map<String, Object> queryAll(CommentQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<Comment> page = new PageInfo<>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), CommentDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }


    @Override
    //@Cacheable
    public List<Comment> queryAll(CommentQueryCriteria criteria){
        return baseMapper.selectList(QueryHelpPlus.getPredicate(Comment.class, criteria));
    }


    @Override
    public void download(List<CommentDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (CommentDto comment : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("用户反馈内容", comment.getComment());
            map.put("创建时间", comment.getCreateTime());
            map.put("创建人", comment.getCreateUser());
            map.put("修改人", comment.getUpdateUser());
            map.put("修改时间", comment.getUpdateTime());
            map.put("删除：0->默认；1->删除", comment.getIsDelete());
            map.put("状态：0->默认；1->禁用", comment.getStatus());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}
