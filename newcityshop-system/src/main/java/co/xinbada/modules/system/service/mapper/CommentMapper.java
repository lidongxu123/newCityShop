/**
* Copyright (C) 2018-2020
* All rights reserved, Designed By www.yixiang.co
* 注意：
* 本软件为www.yixiang.co开发研制
*/
package co.xinbada.modules.system.service.mapper;

import co.xinbada.common.mapper.CoreMapper;
import co.xinbada.modules.system.domain.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* @author lidongxu
* @date 2020-06-23
*/
@Repository
@Mapper
public interface CommentMapper extends CoreMapper<Comment> {

}
