/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.xinbada.co

 */
package co.xinbada.tools.service.mapper;


import co.xinbada.common.mapper.CoreMapper;
import co.xinbada.tools.domain.VerificationCode;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface VerificationCodeMapper extends CoreMapper<VerificationCode> {

}
